// [OBJECTIVE] Create a server-side app using Express Web Framework.
// 🟢🟢🟢 🚨🚨🚨

// Relate this task to something that you do on a daily basis.

// [SECTION] Append the entire app to our node package manager.

	// package.json -> the "heart" of every node project. This also contains different metadata that describes the structure of the project.

	// scripts -> is used to describe custom commands and keyword that can be used to execute this project with the correct runtime environment.

	// NOTE: "start" is globally recognize amongst node projects and frameworks as the 'default' command script to execute a task/project.
	// however for *unconventional* keywords or command you have to append the command "run"
	// SYNTAX: npm run <custom command>

// 1. Identify and prepare the ingredients.
const express = require("express");

// express => will be used as the main component to create the server.
// We need to be able to gather/acquire the utilities and components needed that the express library will provide us.
	// => require() -> directive used to get the library/component needed inside the module

	// prepare the environment in which the project will be served.

// [SECTION] Preparing a Remote repository for ou Node project.

	// NOTE: always DISABLE the node_modules folder
	// WHY?
		// -> it will take up too much space in our repository making it a lot more difficult to stage upon committing the changes in our remote repo.
		// -> if ever that you will deploy your node project on deployment platforms (heroku, netlify, vercel) the project will automatically be rejected because node_modules in not recognized on various deployment platforms.

// [SECTION] Create a Runtime environment that automatically autofix all changes in our app

	// We are going to use a utility called nodemon. (nodemon index)
	// Upon starting the entry point module with nodemon you will be able to the 'append' the application with the proper run time environment. Allowing you to save time and effort upon committing changes to your app.
console.log('This will be our server');
console.log('NEW CHANGES');
console.log('ANOTHER ONE');
// You can even insert items like text art into your run time environments.
console.log(`________00000000000___________000000000000_________
______00000000_____00000___000000_____0000000______
____0000000_____________000______________00000_____
___0000000_______________0_________________0000____
__000000____________________________________0000___
__00000_____________________________________ 0000__
_00000______________________________________00000__
_00000_____________________________________000000__
__000000_________________________________0000000___
___0000000______________________________0000000____
_____000000____________________________000000______
_______000000________________________000000________
__________00000_____________________0000___________
_____________0000_________________0000_____________
_______________0000_____________000________________
_________________000_________000___________________
_________________ __000_____00_____________________
______________________00__00_______________________
________________________00_________________________
`);
console.log(`░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░████████████░░░░░░░░░░░░░░░░░░░
░░░░░░████░░░░░░░░░░░███████░░░░░░░░░░░░
░░░░███░░░░░░░░░░░░░░░░░░░░░██░░░░░░░░░░
░░░██░░░░░░░░░░░░░░░░░░░░░░░░█░░░░░░░░░░
░░█░░░░░░█░░░░░░░░░░░░░░░░░░░█░░░░░░░░░░
░░█░░░█████░░░░░███░░░░░░░░░██░░░░░░░░░░
░░█░░███░███░░░██░░░░░░░░░░████░░░░░░░░░
░░█░░░░░░░░░░░██░░░██░░░░░░█░░█████░░░░░
░░█░░░░░░░░░██░░░░░████░░░░░░░░░░░░██░░░
░░░█░░░░░░░██░░░░░░░░░░░░░░░░░░░░░░░██░░
░░░██░░░░░░██░░░░░░░░░░░░░░░█░░░░░░░░█░░
░░░░██░░░░░░░█████░░░░░░░████░░░░░░░░█░░
░░░░░██░░░░░░░░░░█████████░██░░░░░░░██░░
░░░░░░██░░░░░░░░██░░░██░░░░█░░░░░░░░█░░░
░░░░░░██░░░░░░░██████░░░░░██░░░░░░██░░░░
░░░░░░█░░░░░░░░░░░░░░░░░░██░░░░░░░█░░░░░
░░░░░██░░░░░░░░░░░░░█████░░░░░░░░█░░░░░░
░░░░░█░░░░█░░░░░░░███░░░░░░░░░░░░█░░░░░░
░░░░██░░░███░░░░███░░░░░░░░░░░░░░█░░░░░░
░░░░█░░░░█░█░░░██░░░░░░░░░░░░░░░██░░░░░░
░░░░░░░░██░█░░░██░░░░░░░░░███████░░░░░░░
░░░░░░░░█░░██░░░███░░░░████░░░░░░░░░░░░░
░░░░░░░█░░░░█░░░░░███░░█░░░░░░░░░░░░░░░░
░░░░░░█░░░░░█░░░░░░░░███░░░░░░░░░░░░░░░░
░░░░░░░░░░░░█░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░████░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░██░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
`);
console.log(`Welcome to our Express API Server`);